package ch.cern.monitoring.metadata

import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

protected class MetaDataSets(spark: SparkSession, kafkaInputBrokers: String) {

  import spark.implicits._

  /**
    * Loads the data stream from the requested Kafka topic
    * @param topicName The topic to load the stream from
    * @return DataFrame containing the data from requested topic
    */
  private def metaDataFrames(topicName: String): DataFrame = {
    spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaInputBrokers)
      .option("subscribe", topicName)
      .option("startingOffsets", "earliest")
      .load()
  }


  /**
    * Cric Topology DataSet containing only the most recent record per Name
    */
  def getCricTopology(partitionCols: Seq[String], vo: Option[String]): Dataset[Row] = {
    val CricTopologyRDD = {
      metaDataFrames("cric_raw_topology")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val CricTopologyRDDAggWindow =
      Window.partitionBy(partitionCols.map(x => col(x)): _*)
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dfTopology = spark.read.json(CricTopologyRDD)
                     .select(last($"data").over(CricTopologyRDDAggWindow).as("data"))
                     .dropDuplicates
                     .select($"data.*")

    if (vo.isDefined) {
        dfTopology.where("vo = '" + vo.get + "'")
    }
    dfTopology
  }

  /**
    * Cric Topology Timeseries DataSet containing the full series available in Kafka
    */
  def getCricTopologySeries(vo: Option[String]): Dataset[Row] = {
    val CricTopologyRDD = {
      metaDataFrames("cric_raw_topology")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val dfTopology = spark.read.json(CricTopologyRDD)
                      .dropDuplicates
                      .select($"data.*",$"metadata.timestamp")

    if (vo.isDefined) {
      dfTopology.where("vo = '" + vo.get + "'")
    }
    dfTopology
  }


  /**
    * Rebus Federations DataSet containing only the most recent record per Site
    */
  lazy val rebusFederations: DataFrame = {
    val rebusFederationsRDD = {
      metaDataFrames("rebus_raw_federations")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val rebusAggWindow =
      Window.partitionBy($"data.site")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(rebusFederationsRDD)
      .select(last($"data").over(rebusAggWindow).as("data"))
      .dropDuplicates()
      .select($"data.*")
  }

  /**
    * Agis Topology DataSet containing only the most recent record per ExperimentSite
    */
  lazy val agisTopology: DataFrame = {
    val agisTopologyRDD = {
      metaDataFrames("agis_raw_topology")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val agisTopologyAggWindow = Window.partitionBy($"data.experiment_site")
      .orderBy($"metadata.timestamp")
      .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(agisTopologyRDD)
      .select(last($"data").over(agisTopologyAggWindow).as("data"))
      .dropDuplicates()
      .select($"data.*")
  }

  /**
    * Agis Pandaqueue DataSet containing only the most recent record per Pattern
    */
  lazy val agisPandaqueue: DataFrame = {
    val agisPandaqueueDs =
      metaDataFrames("agis_raw_pandaqueue")
        .select($"value".cast("string"))
        .map(row => row.getAs[String]("value"))

    val agisPandaqueueAggWindow =
      Window.partitionBy($"data.pattern")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(agisPandaqueueDs)
      .select(last($"data").over(agisPandaqueueAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .withColumnRenamed("pattern", "computingsite")
  }

  /**
    * Agis DDM Endpoint DataSet containing only the most recent record per Name
    */
  lazy val agisDDMEndpoint: Dataset[Row] = {
    val agisDDMEndpointRDD = {
      metaDataFrames("agis_raw_ddmendpoint")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val agisDDMEndpointAggWindow =
      Window.partitionBy($"data.name")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(agisDDMEndpointRDD)
      .select(last($"data").over(agisDDMEndpointAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()
  }

  /**
    * CRIC topology extra DataSet containing only the most recent record per Name
    */
  lazy val cricTopologyExtra: Dataset[Row] = {
    val cricTopologyExtraRDD = {
      metaDataFrames("cric_raw_extra")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricTopologyExtraAggWindow =
      Window.partitionBy($"data.experiment_site")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(cricTopologyExtraRDD)
      .select(last($"data").over(cricTopologyExtraAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()
  }

  /**
    * CRIC DDM Endpoint DataSet containing only the most recent record per Name
    */
  lazy val cricDDMEndpoint: Dataset[Row] = {
    val cricDDMEndpointRDD = {
      metaDataFrames("cric_raw_ddmendpoint")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricDDMEndpointAggWindow =
      Window.partitionBy($"data.name")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(cricDDMEndpointRDD)
      .select(last($"data").over(cricDDMEndpointAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()
  }

  /**
    * CRIC Pandaqueue DataSet containing only the most recent record per Name
    */
  lazy val cricPandaQueue: Dataset[Row] = {
    val cricPandaQueueRDD = {
      metaDataFrames("cric_raw_pandaqueue")
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricPandaQueueAggWindow =
      Window.partitionBy($"data.pattern")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    spark
      .read
      .json(cricPandaQueueRDD)
      .select(last($"data").over(cricPandaQueueAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .withColumnRenamed("pattern", "computingsite")
      .dropDuplicates()
  }

  /**
    * Full Topology DataSet containing distinc result from union of Rebus and Agis Datasets
    * In case of duplicated records the Rebus one has higher preference
    */
  lazy val fullTopology: DataFrame = {
    val rebus = rebusFederations.select(
      $"country",
      $"real_federation".as("federation"),
      $"site".as("official_site")
    )

    val agis = agisTopology.select(
      $"country",
      $"federation",
      $"official_site"
    )

    // Fuse together REBUS and AGIS metadata
    // See spark 2.1 docs: .union(..) is equivalent to UNION ALL, we need to drop duplicates here
    rebus.union(agis)
      .groupBy("official_site")
      .agg(
        first($"country").as("country"),
        first($"federation").as("federation"),
        $"official_site")
  }

  /**
    * Topology DataSet that provides flatten result out of the VO or CRIC data records.
    * It return only the most recent records per OfficialSite
    */
  def getFlattenTopology(topic: String, partitionCols: Seq[String], vo: Option[String]): Dataset[Row] = {
    val flattenTopologyRDD = {
      metaDataFrames(topic)
          .select($"value".cast("string"))
          .rdd
          .map(row => row.getAs[String]("value"))
    }

    val metadataAggWindow =
      Window.partitionBy(partitionCols.map(x => col(x)): _*)
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val flattenTopology = spark
      .read
      .json(flattenTopologyRDD)
      .select(last($"data").over(metadataAggWindow).as("data"))
      .dropDuplicates()
      .withColumn("service", explode($"data.services"))
      .select(
        $"service.hostname",
        $"service.flavour".as("service_flavour"),
        $"data.experiment_site",
        $"data.official_site",
        $"data.tier",
        $"data.vo")
    var flattenTopologyFiltered = flattenTopology
    if (vo.isDefined) {
      flattenTopologyFiltered = flattenTopology.where("vo = '" + vo.get + "'")
    }
    flattenTopologyFiltered
  }
}
